import cv2
# from datetime import datetime, timedelta
import time

window_name = "window1"
img_counter = 0
running = True
timer_start = True


def set_up_window():
    """Opens video capture through first available camera and exits if capture cannot be opened.
    Window is set to fullscreen."""
    cap = cv2.VideoCapture(0)
    if not cap.isOpened():
        print("Error: Could not open video.")
        exit()

    cv2.namedWindow(window_name, cv2.WND_PROP_FULLSCREEN)
    cv2.setWindowProperty(window_name, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    return cap


live_capture = set_up_window()


def return_frames(cap):
    """Returns frames and sets waitKey as value other than 0 in order to avoid waiting indefinitely.
    """
    global img_counter
    global timer_start
    while running:
        ret, frame = cap.read()
        k = cv2.waitKey(1)
        if not ret:
            print("Frames not returned.")
            break
        print("waiting")
        cv2.imshow(window_name, frame)
        # esc key is pressed
        if k % 256 == 27:
            print("Exit requested.")
            break
        # space key is pressed
        elif k % 256 == 32:
            time_button_pressed = time.time()
            print(time_button_pressed)
            while timer_start:
                current_time = time.time()
                time_difference = current_time - time_button_pressed
                #print(time_difference)
                print(time_difference)
                cv2.putText(img=frame,
                            text=str(time_difference),
                            org=(0, 100),
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                            fontScale=2,
                            thickness=2,
                            color=(245, 35, 35))
                cv2.imshow(window_name, frame)

                # Take action when timer ends
                if time_difference > 5.0:
                    break
                # if time_difference == 0:
                #     break
            # while img_counter <= 3:
            #     img_name = "photos/opencv_frame_{}.png".format(img_counter)
            #     cv2.imwrite(img_name, frame)
            #     print("{} written!".format(img_name))
            #     img_counter += 1
            #     print(img_counter)


return_frames(live_capture)


def clean(cap):
    """Reads photo taken and displays photo in new image and destroys window."""
    # photo = cv2.imread('photos/opencv_frame_0.png', -1)
    # print("hi")
    # cv2.imshow('window1', photo)
    # print("hello")
    # cv2.waitKey(0)
    cap.release()
    cv2.destroyAllWindows()


clean(live_capture)

